# mmap-quick-setup

Tools to get you up and running with __MMAP__ in a flash with scripts that run through the documented installation process.

__Contents__

- [What is MMAP?](#what-is-mmap)
- [Getting started](#getting-started)
  - [Minimum requirements](#minimum-requirements)
- [Install MMAP and IDOL Media Server](#install-mmap-and-idol-media-server)
- [Ingest video sources](#ingest-video-sources)
  - [Live streaming video](#live-streaming-video)
  - [Video files](#video-files)
- [Video stream sources from the web](#video-stream-sources-from-the-web)
  - [Arabic](#arabic)
  - [Chinese](#chinese)
  - [English](#english)
  - [German](#german)
  - [Italian](#italian)
  - [Spanish](#spanish)
  - [Urdu](#urdu)

## What is MMAP?

MMAP, the *Media Management and Analytics Platform*, provides a set to tools to support the recording, analysis and presentation of video streams using IDOL Media Server.  MMAP is built on JBoss and includes the following elements:

- A RESTful API for the configuration of channels, recorders and analytics as well as for accessing of alert result data.
- An object store, where channel and recorder (*i.e.* Media Server) configurations are stored.
- Connections to a PostgreSQL database, where analytics results are stored.
- Sample "widgets" (HTML, JavaScript, CSS) that access this API to display data.
- A Chrome [native client](https://code.google.com/archive/p/ppapi/) plugin video player that allows advanced playback functionality from our rolling buffer.
- A sample web application that integrates these widgets and the player:

![mmap-ui](figs/mmap-ui.png)

## Getting started

### Minimum requirements

- 2 cores, 4 GB RAM and 20GB free disk space.
- 64-bit Windows or Linux (this guide has been most recently tested on Windows 10 and Ubuntu 18.10).
- A text editor, *e.g.* [VS Code](https://code.visualstudio.com/).
- Administrator privileges to install software.

## Install MMAP and IDOL Media Server

Follow [these steps](install/INSTALL.md) to install MMAP and its dependencies.

## Ingest video sources

The [MMAP quickstart](localhost:8080) guide provides step-by-step instructions for video ingestion but for a guided walk-through, see the following sections. 

### Live streaming video

Follow [these steps](setup/LIVE.md) to prepare MMAP to manage streaming video.

### Video files

While you can also use the above steps to process video files (effectively as segments of streams), as of MMAP version 12.1, a new and better workflow exists.  Follow [these steps](setup/FILE.md).

# Appendix

## Video stream sources from the web

There exist many free news streams on the web that you can connect to.  The following channels were identified by manually inspecting websites and were working at time of writing.

### Arabic

Broadcaster | Resolution | Link
--- | --- | ---
BBC News | 640x360 | http://bbcwshdlive01-lh.akamaihd.net/i/atv_1@61433/index_800_av-p.m3u8

### Chinese

Broadcaster | Resolution | Link
--- | --- | ---
CCTV 13 | 720x576 | http://223.110.243.172/PLTV/2510088/224/3221227168/1.m3u8

### English

Broadcaster | Resolution | Link
--- | --- | ---
Al Jazeera | 960x540 | http://aljazeera-eng-hd-live.hls.adaptive.level3.net/aljazeera/english2/index1296.m3u8
C-SPAN 1 | 1024x576 | http://cspan1-lh.akamaihd.net/i/cspan1_1@304727/index_1000_av-p.m3u8
C-SPAN 2 | 1024x576 | http://cspan2-lh.akamaihd.net/i/cspan2_1@304728/index_1000_av-p.m3u8

### German

Broadcaster | Resolution | Link
--- | --- | ---
DW | 720x400 | http://dwstream72-lh.akamaihd.net/i/dwstream72_live@123556/master.m3u8

### Italian

Broadcaster | Resolution | Link
--- | --- | ---
Rai News 24 | 704x396 | http://b2everyrai-lh.akamaihd.net/i/rainews_1@179616/index_1800_av-p.m3u8

### Spanish

Broadcaster | Resolution | Link
--- | --- | ---
Milenio TV | 320x240 | http://bcoveliveios-i.akamaihd.net/hls/live/201661/57828478001/milenio_center_512k@51752.m3u8

### Urdu

Broadcaster | Resolution | Link
--- | --- | ---
Geo TV | 640x360 | http://stream1.jeem.tv/geo/geonews_abr/playlist.m3u8
