@echo off

REM ===========================================================================
REM  MMAP runner
REM ===========================================================================

set VERSION=12.2.0
set WILDFLY=wildfly-10.1.0.Final
set WILDFLY_BASE_DIR=C:\MicroFocus\MMAP-%VERSION%\%WILDFLY%

start "MMAP" %WILDFLY_BASE_DIR%\bin\standalone.bat -c avalanche.xml -P=%WILDFLY_BASE_DIR%\avalanche.properties

echo ---------------------------------
echo MMAP has started in a new window.
pause
