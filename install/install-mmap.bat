@echo off

REM ===========================================================================
REM  MMAP installer
REM ===========================================================================
pushd %~dp0

REM Variables
set VERSION=12.2.0
set WILDFLY=wildfly-10.1.0.Final
set POSTGRES_BIN=C:\Progra~1\PostgreSQL\11\bin
set POSTGRES_JAR=postgresql-42.2.5.jar

REM ---------------------------------------------------------------------------
echo + Extracting zips

set WORKING_DIR=C:\MicroFocus\MMAP-%VERSION%
set MMAP_ZIP=mmap-all-wildfly-%VERSION%.zip
set WILDFLY_BASE_DIR=%WORKING_DIR%\%WILDFLY%

echo ++ %WILDFLY%.zip ...
powershell Expand-Archive %WORKING_DIR%\%WILDFLY%.zip -DestinationPath %WORKING_DIR%

echo ++ %MMAP_ZIP% ...
powershell Expand-Archive %WORKING_DIR%\%MMAP_ZIP% -DestinationPath %WILDFLY_BASE_DIR% -Force

REM ---------------------------------------------------------------------------
echo + Configuring PostgreSQL

set POSTGRES_DIR=%WILDFLY_BASE_DIR%\modules\org\postgresql\main
md %POSTGRES_DIR%

copy %WORKING_DIR%\%POSTGRES_JAR% %POSTGRES_DIR%
copy module.xml %POSTGRES_DIR%

set SQL_DIR=%WILDFLY_BASE_DIR%\sql\postgresql
%POSTGRES_BIN%\psql.exe -c "CREATE DATABASE events;" -U postgres
%POSTGRES_BIN%\psql.exe -f %SQL_DIR%\schema.sql -d events -U postgres
%POSTGRES_BIN%\psql.exe -f %SQL_DIR%\stored-procedures.sql -d events -U postgres
%POSTGRES_BIN%\psql.exe -f %SQL_DIR%\searchable-text.sql -d events -U postgres
%POSTGRES_BIN%\psql.exe -f %SQL_DIR%\partition-word-table.sql -d events -U postgres

REM ---------------------------------------------------------------------------
echo + Configuring MMAP

mkdir %WORKING_DIR%\incomingBinaryData
mkdir %WORKING_DIR%\binaryData
mkdir %WORKING_DIR%\temp
copy avalanche.windows.properties %WILDFLY_BASE_DIR%\avalanche.properties

REM ---------------------------------------------------------------------------
echo ------------------------
echo MMAP has been installed.
popd
pause
