#!/usr/bin/env bash

# ===========================================================================
# MMAP runner
# ===========================================================================

VERSION=12.2.0
WILDFLY=wildfly-10.1.0.Final
WILDFLY_BASE_DIR=/opt/mmap/$WILDFLY

$WILDFLY_BASE_DIR/bin/standalone.sh -c avalanche.xml -P=$WILDFLY_BASE_DIR/avalanche.properties

echo ---------------------------------
echo MMAP has started in a new window.
