@echo off

REM ===========================================================================
REM  MMAP uninstaller
REM ===========================================================================
pushd %~dp0

REM Variables
set VERSION=12.2.0
set WILDFLY=wildfly-10.1.0.Final
set POSTGRES_BIN=C:\Progra~1\PostgreSQL\11\bin

REM ---------------------------------------------------------------------------
echo - Removing PostgreSQL configuration

%POSTGRES_BIN%\psql.exe -c "DROP DATABASE events;" -U postgres

REM ---------------------------------------------------------------------------
echo - Removing software

set WORKING_DIR=C:\MicroFocus\MMAP-%VERSION%

rd /s /q %WORKING_DIR%\incomingBinaryData
rd /s /q %WORKING_DIR%\binaryData
rd /s /q %WORKING_DIR%\temp
rd /s /q %WORKING_DIR%\%WILDFLY%

REM ---------------------------------------------------------------------------
echo ----------------------
echo MMAP has been removed.
popd
pause
