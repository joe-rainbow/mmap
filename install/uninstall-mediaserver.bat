@echo off

REM ===========================================================================
REM  MMAP Playlist and Analytics Server uninstaller
REM ===========================================================================
REM When launched by an Administrator, still run in current directory
pushd %~dp0

set VERSION=12.2.0
set WORKING_DIR=C:\MicroFocus\MMAP-%VERSION%

REM ---------------------------------------------------------------------------
echo - Removing services
net stop MMAP-MediaServer
net stop MMAP-PlaylistServer
timeout 5
sc delete MMAP-MediaServer
sc delete MMAP-PlaylistServer

REM ---------------------------------------------------------------------------
echo - Removing software
rmdir /s /q %WORKING_DIR%\MediaServer_%VERSION%_WINDOWS_X86_64
rmdir /s /q %WORKING_DIR%\PlaylistServer_%VERSION%_WINDOWS_X86_64

REM ---------------------------------------------------------------------------
echo ----------------------------------------------------------
echo Playlist and Analytics Servers for MMAP have been removed.
popd
pause
