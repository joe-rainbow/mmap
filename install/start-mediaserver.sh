#!/bin/bash

# Need to start from mediaserver directory
# pushd "$(dirname "$0")" > /dev/null 2>&1
pushd $(dirname "${0}") > /dev/null

SERVERNAME=mediaserver
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH":`pwd`/libs:`pwd`/../common/runtime

wait_for_start() {
  # Wait for the pid file to be created, this doesn't occur until after 
  #  the license is acquired so wait ~30s
  count=0
  started=0
  until [ $count -gt 30 ] ; do 
    if [ -f "$SERVERNAME.pid" ] ; then
      started=1
	  break
    fi
    sleep 1
    count=$((count+1))
  done

  if [ $started = 0 ]; then
    echo "Failed to start $SERVERNAME"
    exit 1
  fi
}

start_server() {
  if [ -f "$SERVERNAME.pid" ] ; then
    pid=`cat $SERVERNAME.pid`
    if ps -p $pid > /dev/null; then
      echo "$SERVERNAME already running"
      exit 0
    else
      rm $SERVERNAME.pid
    fi
  fi

  chmod u+x $SERVERNAME.exe
  echo "Starting $SERVERNAME"
  nohup ./$SERVERNAME.exe > /dev/null 2>&1 &

  wait_for_start
  echo "Started $SERVERNAME"
}

start_server

popd > /dev/null
