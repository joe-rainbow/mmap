#!/usr/bin/env bash

# ===========================================================================
#  MMAP Playlist and Analytics Server uninstaller
# ===========================================================================
pushd $(dirname "${0}") > /dev/null

VERSION=12.2.0
WORKING_DIR=/opt/mmap

# ---------------------------------------------------------------------------
echo - Stopping services
$WORKING_DIR/MediaServer_$VERSION"_LINUX_X86_64/stop-mediaserver.sh"
$WORKING_DIR/PlaylistServer_$VERSION"_LINUX_X86_64/stop-mediaserver.sh"
sleep 5

# ---------------------------------------------------------------------------
echo - Removing software
rm -rf $WORKING_DIR/MediaServer_$VERSION"_LINUX_X86_64"
rm -rf $WORKING_DIR/PlaylistServer_$VERSION"_LINUX_X86_64"

# ---------------------------------------------------------------------------
echo ----------------------------------------------------------
echo Playlist and Analytics Servers for MMAP have been removed.
popd > /dev/null
