#!/usr/bin/env bash

# ===========================================================================
# MMAP uninstaller
# ===========================================================================

# Variables
VERSION=12.2.0
WILDFLY=wildfly-10.1.0.Final

# ---------------------------------------------------------------------------
echo - Removing PostgreSQL configuration
su postgres -c "psql -c \"DROP DATABASE events;\""

# ---------------------------------------------------------------------------
echo - Removing software

WORKING_DIR=/opt/mmap

rm -rf $WORKING_DIR/incomingBinaryData
rm -rf $WORKING_DIR/binaryData
rm -rf $WORKING_DIR/temp
rm -rf $WORKING_DIR/$WILDFLY

# ---------------------------------------------------------------------------
echo ----------------------
echo MMAP has been removed.
