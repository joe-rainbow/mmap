@echo off

REM ===========================================================================
REM  MMAP Playlist and Analytics Server installer
REM ===========================================================================
pushd %~dp0

set VERSION=12.2.0
set WORKING_DIR=C:\MicroFocus\MMAP-%VERSION%
set MEDIA_SERVER_DIR=%WORKING_DIR%\MediaServer_%VERSION%_WINDOWS_X86_64
set PLAYLIST_SERVER_DIR=%WORKING_DIR%\PlaylistServer_%VERSION%_WINDOWS_X86_64

REM ---------------------------------------------------------------------------
echo + Extracting zips

echo ++ MediaServer_%VERSION%_WINDOWS_X86_64.zip ...
powershell Expand-Archive %MEDIA_SERVER_DIR%.zip -DestinationPath %WORKING_DIR%
robocopy %MEDIA_SERVER_DIR% %PLAYLIST_SERVER_DIR% /S /E

echo ++ ENUK-%VERSION%.zip ...
mkdir %MEDIA_SERVER_DIR%\staticdata\speechtotext
unzip -q %WORKING_DIR%\ENUK-%VERSION%.zip -d %MEDIA_SERVER_DIR%\staticdata\speechtotext\

REM ---------------------------------------------------------------------------
echo + Configuring servers
copy analyticsserver.cfg %MEDIA_SERVER_DIR%\mediaserver.cfg
copy playlistserver.cfg %PLAYLIST_SERVER_DIR%\mediaserver.cfg

echo ++ Running Visual C++ redistributibles...
%MEDIA_SERVER_DIR%\runtime\vcredist_2010.exe /passive /norestart
%MEDIA_SERVER_DIR%\runtime\vcredist_2013.exe /install /passive /norestart
%MEDIA_SERVER_DIR%\runtime\vcredist_x86_2013.exe /install /passive /norestart

echo ++ Creating Windows services...
%MEDIA_SERVER_DIR%\mediaserver.exe -install -servicename MMAP-MediaServer -start manual
%PLAYLIST_SERVER_DIR%\mediaserver.exe -install -servicename MMAP-PlaylistServer -start manual

REM ---------------------------------------------------------------------------
echo ------------------------------------------------------------
echo Playlist and Analytics Servers for MMAP have been installed.
popd
pause
