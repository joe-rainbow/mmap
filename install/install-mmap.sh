#!/usr/bin/env bash

# ===========================================================================
# MMAP installer
# ===========================================================================
pushd $(dirname "${0}") > /dev/null

# Variables
VERSION=12.2.0
WILDFLY=wildfly-10.1.0.Final
POSTGRES_JAR=postgresql-42.2.5.jar

# ---------------------------------------------------------------------------
echo + Extracting zips

WORKING_DIR=/opt/mmap
MMAP_ZIP=mmap-all-wildfly-$VERSION".zip"
WILDFLY_BASE_DIR=$WORKING_DIR/$WILDFLY

echo ++ $WILDFLY.zip ...
unzip -q $WORKING_DIR/$WILDFLY.zip -d $WORKING_DIR

echo ++ $MMAP_ZIP ...
unzip -q -o $WORKING_DIR/$MMAP_ZIP -d $WILDFLY_BASE_DIR

# ---------------------------------------------------------------------------
echo + Configuring PostgreSQL

POSTGRES_DIR=$WILDFLY_BASE_DIR/modules/org/postgresql/main
mkdir -p $POSTGRES_DIR

cp $WORKING_DIR/$POSTGRES_JAR $POSTGRES_DIR/
cp module.xml $POSTGRES_DIR/

SQL_DIR=$WILDFLY_BASE_DIR/sql/postgresql
su postgres -c "psql -c \"CREATE DATABASE events;\""
su postgres -c "psql -f $SQL_DIR/schema.sql -d events"
su postgres -c "psql -f $SQL_DIR/stored-procedures.sql -d events"
su postgres -c "psql -f $SQL_DIR/searchable-text.sql -d events"
su postgres -c "psql -f $SQL_DIR/partition-word-table.sql -d events"

# ---------------------------------------------------------------------------
echo + Configuring MMAP

mkdir $WORKING_DIR/incomingBinaryData
mkdir $WORKING_DIR/binaryData
mkdir $WORKING_DIR/temp
cp avalanche.linux.properties $WILDFLY_BASE_DIR/avalanche.properties

# ---------------------------------------------------------------------------
echo ------------------------
echo MMAP has been installed.
popd > /dev/null
