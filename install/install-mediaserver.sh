#!/usr/bin/env bash

# ===========================================================================
#  MMAP Playlist and Analytics Server installer
# ===========================================================================
pushd $(dirname "${0}") > /dev/null

VERSION=12.2.0
WORKING_DIR=/opt/mmap
MEDIA_SERVER_DIR=$WORKING_DIR/MediaServer_$VERSION"_LINUX_X86_64"
PLAYLIST_SERVER_DIR=$WORKING_DIR/PlaylistServer_$VERSION"_LINUX_X86_64"

# ---------------------------------------------------------------------------
echo + Extracting zips

echo ++ MediaServer_$VERSION"_LINUX_X86_64".zip ...
unzip -q $MEDIA_SERVER_DIR.zip -d $WORKING_DIR
cp -p start-mediaserver.sh $MEDIA_SERVER_DIR/
cp -rp $MEDIA_SERVER_DIR $PLAYLIST_SERVER_DIR

echo ++ ENUK-$VERSION.zip ...
mkdir $MEDIA_SERVER_DIR/staticdata/speechtotext
unzip -q $WORKING_DIR/ENUK-$VERSION.zip -d $MEDIA_SERVER_DIR/staticdata/speechtotext/

# ---------------------------------------------------------------------------
echo + Configuring servers
cp analyticsserver.cfg $MEDIA_SERVER_DIR/mediaserver.cfg
cp playlistserver.cfg $PLAYLIST_SERVER_DIR/mediaserver.cfg

# ---------------------------------------------------------------------------
echo ------------------------------------------------------------
echo Playlist and Analytics Servers for MMAP have been installed.
popd > /dev/null
