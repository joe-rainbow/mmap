# Install MMAP

Steps to obtain and setup MMAP and its dependencies.

__Contents__

- [Obtain IDOL software](#obtain-idol-software)
  - [If you are a Micro Focus employee or partner](#if-you-are-a-micro-focus-employee-or-partner)
  - [Otherwise](#otherwise)
- [Installation](#installation)
  - [Prerequisites](#prerequisites)
  - [WildFly](#wildfly)
    - [Java Runtime Environment](#java-runtime-environment)
      - [Windows](#windows)
      - [Ubuntu](#ubuntu)
    - [PostgreSQL database](#postgresql-database)
      - [Windows](#windows-1)
      - [Ubuntu](#ubuntu-1)
    - [JDBC driver](#jdbc-driver)
  - [MMAP](#mmap)
    - [Installation scripts](#installation-scripts)
      - [Windows](#windows-2)
      - [Ubuntu](#ubuntu-2)
    - [Verify that MMAP is running](#verify-that-mmap-is-running)
  - [IDOL Media Server](#idol-media-server)
    - [Installation scripts](#installation-scripts-1)
      - [Windows](#windows-3)
      - [Ubuntu](#ubuntu-3)
    - [Licensing](#licensing)
    - [Run](#run)
      - [Windows](#windows-4)
      - [Ubuntu](#ubuntu-4)
    - [Verify that both Media Servers are running](#verify-that-both-media-servers-are-running)

## Obtain IDOL software

### If you are a Micro Focus employee or partner

You can obtain software and a testing license from the Micro Focus [MySupport portal](https://softwaresupport.softwaregrp.com/).

1. Select *IDOL*, then *IDOL Media Management and Analysis Platform SW E-Media*
2. Complete the web form with your details to gain access
3. Go to the *Get Software* tab and download:
   - `mmap-all-wildfly-12.2.0.zip` and
   - `MediaServer_12.2.0_WINDOWS_X86_64.zip` or `MediaServer_12.2.0_LINUX_X86_64.zip`.
   - `ENUK-12.2.0.zip`.

Copy them all into `C:\MicroFocus\MMAP-12.2.0` (Windows) or `/opt/mmap` (Ubuntu).

### Otherwise

Please check with the Micro Focus contact who provided you with these tutorials to request the required software.

## Installation

### Prerequisites

MMAP relies on a number of third party packages and applications.  In this section we will walk through their setup.

### WildFly

Download the WildFly aapplication server:

- [wildfly-10.1.0.Final.zip](https://download.jboss.org/wildfly/10.1.0.Final/wildfly-10.1.0.Final.zip)

Copy this `.zip` alongside the MMAP installation zip, *i.e.* in `C:\MicroFocus\MMAP-12.2.0` (Windows) or `/opt/mmap` (Ubuntu).

#### Java Runtime Environment

Java is required to run WildFly, and so MMAP.

##### Windows

The following version has been most recently tested:

- [jre-8u77-windows-x64.exe](http://www.oracle.com/technetwork/java/javase/downloads/java-archive-javase8-2177648.html)

Set your JAVA_HOME environment variable, *e.g.* with the following command executed in CMD as Administrator:

```cmd
setx /M JAVA_HOME C:\Progra~1\Java\jre1.8.0_77
```

##### Ubuntu

Use the package manager to install OpenJDK, then verify the installation.  The following version has been most recently tested:

```bsh
$ sudo apt install openjdk-8-jre
$ java -version
openjdk version "1.8.0_191"
OpenJDK Runtime Environment (build 1.8.0_191-8u191-b12-0ubuntu0.18.10.1-b12)
OpenJDK 64-Bit Server VM (build 25.191-b12, mixed mode)
```

If the version returned does not match what you just installed, you may have multiple version of Java on your system. Use the following command to chose the version we just installed as follows:

```bsh
$ sudo update-alternatives --config java
There are 2 choices for the alternative java (providing /usr/bin/java).

  Selection    Path                                            Priority   Status
------------------------------------------------------------
* 0            /usr/lib/jvm/java-11-openjdk-amd64/bin/java      1111      auto mode
  1            /usr/lib/jvm/java-11-openjdk-amd64/bin/java      1111      manual mode
  2            /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java   1081      manual mode

Press <enter> to keep the current choice[*], or type selection number: 2
```

#### PostgreSQL database

The PostgreSQL is used by MMAP to store and retrieve alert data from analytics.

##### Windows

Version 11.1 has been most recently tested:

- [postgresql-11.1-1-windows-x64.exe](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads)

Run the installer, keeping to the defaults.  The following assumptions are made later in the configuration:

1. PostgreSQL will listen on port `5432`, and
1. your superuser password is `postgres`.

After installation, the `postgresql-x64-11` Windows service will start automatically.

##### Ubuntu

Use the package manager to install:

```bsh
$ sudo apt install postgresql
```

After installation, the PostgreSQL service will start automatically.  A new user `postgres` will have also been created.  Access the access a PostgreSQL prompt as this user:

```bsh
$ sudo -u postgres psql
psql (10.6 (Ubuntu 10.6-0ubuntu0.18.10.1))
Type "help" for help.

postgres=# \password postgres
postgres=# \q
```

PostgreSQL has been successfully installed. 

Finally, set a password at the prompt for the `postgres` user by typing `\password postgres` (we later assume you set this to `postgres`), then exit the prompt with `\q`.

#### JDBC driver

The JDBC driver allows MMAP to communicate with PostgreSQL.

Download the PostgreSQL JDBC driver.  Version 42.2.5 has been most recently tested:

- [postgresql-42.2.5.jar](https://jdbc.postgresql.org/download/postgresql-42.2.5.jar)

Copy the `.jar` file into `C:\MicroFocus\MMAP-12.2.0` (Windows) or `/opt/mmap` (Ubuntu).

### MMAP

#### Installation scripts

Run the included scripts to carry out the installation steps an then start MMAP.

##### Windows

Run `install-mmap.bat` with administrator privileges:

- right click
- select 'Run as administrator'

When prompted, enter your PostgreSQL super user password, which you noted down earlier.

Run `run-mmap.bat` with administrator privileges:

- right click
- select 'Run as administrator'

##### Ubuntu

The installation script uses `unzip`.  If you do not have this installed, first install through the package manager:

```bsh
sudo apt update
sudo apt install unzip
```

Run the following commands to execute the installation and startup scripts:

```bsh
cd install
sudo chmod +x *.sh
sudo ./install-mmap.sh
sudo ./run-mmap.sh
```

#### Verify that MMAP is running

Point your favorite browser to the following link to explore MMAP: <http://localhost:8080>.

Once fully deployed, going to this page will display the object store content as JSON: <http://localhost:8080/vms/api/v1/items>.

### IDOL Media Server

In order to work with video sources, we also need IDOL Media Server.  MMAP makes use of Media Server in two ways:

- Playlist Server: to create clips, images and play lists from video files
- Analytics Server: to record and analyse video sources

Here we will install two separate instances of Media Server to perform these roles.

#### Installation scripts

##### Windows

Run `install-mediaserver.bat` with administrator privileges:

- right click
- select 'Run as administrator'

##### Ubuntu

Run the following command to execute the installation script:

```bsh
sudo ./install-mediaserver.sh
```

#### Licensing

IDOL Media Server must be licensed to run.  This guide assumes you have an IDOL License Server installed and running on `localhost` port `20000`.  If your system is different, edit the Media Server configuration files to point to your License Server.

Media Server is licensed by analytic type and by number of concurrent analytics.  Note the following sections for the Playlist and Analytics server configuration files, allowing respectively video recording management and speech analytics:

```ini
[Modules]
# Enable=audiocategorize,audiomatch,barcode,demographics,facedetect,facestate,facerecognize,imageclassification,imagecomparison,imagehash,languageid,numberplate,objectclassrecognition,objectrecognition,ocr,speakerid,speechtotext,vehiclemodel
Enable=

[Channels]
# Make sure enough license channels are available to cover Process threads
VisualChannels=0
SurveillanceChannels=0
AudioChannels=0
VideoManagementChannels=1
```

```ini
[Modules]
# Enable=audiocategorize,audiomatch,barcode,demographics,facedetect,facestate,facerecognize,imageclassification,imagecomparison,imagehash,languageid,numberplate,objectclassrecognition,objectrecognition,ocr,speakerid,speechtotext,vehiclemodel
Enable=speakerid,speechtotext

[Channels]
# Make sure enough license channels are available to cover Process threads
VisualChannels=0
SurveillanceChannels=0
AudioChannels=1
VideoManagementChannels=1
```

To run additional analytics, those modules must be enabled.  Full details of the licensing scheme are provided [here](https://www.microfocus.com/documentation/idol/IDOL_12_1/MediaServer/Help/index.html#Configuration/Channels/Channels.htm).

If you are not already familiar with Media Server and for more in-depth instruction on getting set up with Media Server see the [rich media tutorials](https://github.houston.softwaregrp.net/bdp-rich-media-demos/rich-media-tutorials).

#### Run

Start up both Media Server instances.

##### Windows

Start the new Windows Services `MMAP-MediaServer` and `MMAP-PlaylistServer`.

##### Ubuntu

Run the following commands to execute the startup scripts:

```bsh
sudo /opt/mmap/MediaServer_12.2.0_LINUX_X86_64/start-mediaserver.sh
sudo /opt/mmap/PlaylistServer_12.2.0_LINUX_X86_64/start-mediaserver.sh
```

#### Verify that both Media Servers are running

Point your favorite browser to the following links to check the status of each Media Server: 

- Playlist Server: <http://localhost:14100/a=getStatus>
- Analytics Server: <http://localhost:14000/a=getStatus>

---

Return to top-level [README](../README.md).
