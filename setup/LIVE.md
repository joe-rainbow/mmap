# Configure MMAP to process live video streams

Steps to prepare MMAP to manage streaming video (and files if you must).

__Contents__

- [Link to Media Server](#link-to-media-server)
  - [Configure an ODBC driver](#configure-an-odbc-driver)
    - [Windows](#windows)
    - [Ubuntu](#ubuntu)
- [Get up and running](#get-up-and-running)
  - [Quickstart with Postman](#quickstart-with-postman)
    - [Channel icons](#channel-icons)
  - [Loading multiple channels with deploy tool](#loading-multiple-channels-with-deploy-tool)
    - [Prepare](#prepare)
    - [Deploy](#deploy)

## Link to Media Server

In this mode of operation, MMAP directs Media Server to process your source, ensuring that Media Server creates all the assets MMAP needs.

### Configure an ODBC driver

Media Server will need to add data to MMAP's database.  To allow, this an ODBC driver must be configured on the machine where Media Server will run.

#### Windows

Run the PostgreSQL ODBC driver installer with defaults.  The following version has been most recently tested:

- [psqlodbc_09_03_0400.zip](https://ftp.postgresql.org/pub/odbc/versions/msi/psqlodbc_09_03_0400.zip) (*direct link*)

With MMAP running, open the Windows ODBC management utility:

- In the Start menu search "ODBC", then
- select `ODBC Data Sources (64-bit)`

Configure your connection:

- On the 'System DSN' tab, click `Add...` and choose the driver labelled `PostgreSQL Unicode(x64)`.

- Set the fields as shown in this image:

  ![odbc-mmap](figs/odbc-mmap.png)

- The "Data Source" name can be anything you want, we will later assume it is `IDOL_MMAP`.
- The "Server" host should be the name of the machine where MMAP is installed.
- The hidden "Password" should be your PostgreSQL super user password, which you noted down earlier.

Click __Test__ and you will see your connection is successful.

#### Ubuntu

Use the package manager to install the PostgreSQL ODBC driver:

```bsh
sudo apt install odbc-postgresql
```

Edit the following configuration to fill in your PostgreSQL super user password, which you noted down earlier, then copy the text into `/etc/odbc.ini`:

```ini
[ODBC Data Sources]
IDOL_MMAP = "IDOL_MMAP"

[IDOL_MMAP]
Driver = /usr/lib/x86_64-linux-gnu/odbc/psqlodbcw.so
Protocol = 10.6
Database = events
Servername = localhost
UserName = postgres
Password = <PASSWORD>
Port = 5432
ByteaAsLongVarBinary = 1
```

Finally, run the following command to test the connection:

```bsh
$ isql -v "IDOL_MMAP" postgres <PASSWORD>
+---------------------------------------+
| Connected!                            |
|                                       |
| sql-statement                         |
| help [tablename]                      |
| quit                                  |
|                                       |
+---------------------------------------+
SQL> quit
```

The connection with PostgreSQL has been successfully established. Exit the prompt by typing `quit` then pressing enter.

## Get up and running

This guide assumes that you have already installed MMAP and IDOL Media Server following [this guide](../install/INSTALL.md) and that MMAP and both the analytics and playlist Media Server instances are running.

In this section we introduce two options for configuring MMAP to process live video streams:

1. Using the Postman tool to step through the setup of a single channel.
2. Running a node.js deployment tool to configure multiple sources (including video files).

### Quickstart with Postman

The MMAP welcome page includes an easy-to-follow quickstart guide to get up and running with video analytics results in [MMAP](http://localhost:8080/##quickstart).

To make this process easier still, you can make use of the Postman REST client to launch the quickstart steps, ingesting Al Jazeera English.

- Install [Postman](https://www.getpostman.com/)
- Import the package from this project: Click `Import` > `Choose Files`
- Select this Postman collection `setup/MMAP 12.2.0 Quick Start.postman_collection.json`

This collection includes a folder `Live` with actions for each of the quickstart steps.  To learn more about the options available, read the MMAP [API](http://localhost:8080/docs/microfocus-mmap-api.html).

#### Channel icons

A channel icon is added to MMAP as a base64-string encoding of the image data in the create channel action.  This package includes my small web app (`thumbnails/b64thumbnailTool.html`) that uses the HTML `<canvas>` element to create a base-64 string for you from your own image.  Open the `.html` file in your browser (_tested in Google Chrome_) then import an image by clicking 'choose file'.  The page will display a square thumbnail and write out a base-64 string representation of it.  Copy that string and paste it over the one already set in the Postman collection `Live/2. Create a channel`, *e.g.*

```json
{
  "entityType": "Channel",
  "properties": {
    "logoSrc": "<PASTE IMAGE DATA STRING HERE>"
  }
}
```

### Loading multiple channels with deploy tool

A simple deployment tool is also included here to automate the same steps included in the Postman collection to setup and start processing multiple channels.

If you would like to use this, first install __node.js__ for your system from the [official downloads page](https://nodejs.org/en/download/).  Next, download project dependencies from the command line:

```bsh
npm install
```

#### Prepare

Set up your channel options in `setup/config.js`, *e.g.* for Al Jazeera English:

```js
{
  name: 'Al Jazeera',
  type: 'Channel',
  src: 'http://aljazeera-eng-hd-live.hls.adaptive.level3.net/aljazeera/english2/index1296.m3u8',
  persist: true,
  cfg: [
    { template: 'record' },
    { template: 'faces' },
    { template: 'speakers' }
  ],
  maxFiles: 5,
  logoSrc: 'data:image/png;base64,iVBORw0K...'
}
```

Next, choose from the included configuration files from (or add your own into) the `setup/cfg` directory.  These configuration files must follow the standard MMAP config formatting and combine some common settings, see the [API](http://localhost:8080/docs/microfocus-mmap-api.html) for details.  They can combined by the deploy tool to give you some added flexibility. If you select a config template, you can optionally override particular variables, *e.g.*

```js
cfg: [
  { template: 'record' },
  { template: 'faces' },
  {
    template: 'speakers',
    override: [ { speedBias: '4' } ]
  }
]
```

#### Deploy

To run the deployment script start and select your deployment tasks:

```bsh
cd setup/deploy
node app.js
```

You will be prompted to select which input(s) to configure then which action to run: `Setup`, `Start`, `Stop` or `Clear`.

---

Return to top-level [README](../README.md).
