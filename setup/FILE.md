# Process video files for export to MMAP

Steps to process a video file with IDOL Media Server to push the results to MMAP for display.

__Contents__

- [Link from Media Server](#link-from-media-server)
- [Get up and running](#get-up-and-running)
  - [Quickstart with Postman](#quickstart-with-postman)
  - [Custom configuration](#custom-configuration)

## Link from Media Server

In this mode of operation, MMAP will receive analytics results from Media Server at the end of processing your source video file. MMAP employs a second Media Server instances, referred to as the "Playlist Server", which is used to stream video and create clips and snapshots, as may be requested by the MMAP API.

## Get up and running

This guide assumes that you have already installed MMAP and IDOL Media Server following [this guide](../install/INSTALL.md) and that MMAP and both the analytics and playlist Media Server instances are running.

In this section we introduce two options for configuring Media Server to process video files for MMAP:

1. Using the Postman tool to step through the setup of a single channel.
1. Manual configuration from the default configuration file.

Download [this recording](http://tech-demo.idol.swinfra.net/tutorial/aljazeera.mp4) from Al Jazeera English and save it under `C:\MicroFocus\video` (Windows) or `/opt/mmap/video` (Ubuntu)..

### Quickstart with Postman

The MMAP welcome page includes an easy-to-follow quickstart guide to get up and running with video analytics results in [MMAP](http://localhost:8080/##quickstart).

To make this process easier still, you can make use of the Postman REST client to launch the quickstart steps, ingesting a video clip of Al Jazeera English.

- Install [Postman](https://www.getpostman.com/)
- Import the package from this project: Click `Import` > `Choose Files`
- Select this Postman collection `setup/MMAP 12.1.0 Quick Start.postman_collection.json`

This collection includes a folder `File` with a Media Server action to launch processing as well as MMAP utility actions to list or delete videos.  To learn more about other available actions, read the MMAP [API](http://localhost:8080/docs/microfocus-mmap-api.html).

Data are not sent to MMAP until the video processing is complete.  While processing, you can monitor the analytics through Media Server's [Activity web app](http://localhost:14000/action=activity).  Once complete, go to the [MMAP media application](http://localhost:8080/media/videos?video=aljazeera.mp4) to view the results.

### Custom configuration

You IDOL Media Server installation contains a set of example process configuration files, which you can find under `configurations/examples`.  This set includes a "master" MMAP processing configuration `MMAP/videoIngest.cfg`, which includes example configuration sections for all the analytics currently supported by MMAP, *i.e.* that can currently be stored by MMAP in its PostgreSQL database.  For each analytic there are instructions included as to how to modify this file.

The example configuration file you used above in the Postman collection was created from this master file to run video encoding, keyframe analysis, speaker ID and speech to text.  You can compare this configuration, *e.g.* `videoIngest.linux.cfg`, to the master to see the changes that have been made.

Once you have created a new process configuration and are ready to run it, you can either:

1. modify the `Process video` action in the Postman collection, or
1. launch the process action through your preferred tool.  The action is specified in these [docs](https://www.microfocus.com/documentation/idol/IDOL_12_1/MediaServer/Help/index.html#Actions/VideoAnalysis/Process.htm).

If you choose to modify the Postman action, you must convert your new configuration file into a base64-encoded string.  You can either find a web site to do this for you or use the `base64` command-line tool available on Ubuntu (or from [Git BASH](https://gitforwindows.org/) on Windows) as follows: 

```bsh
base64 --wrap=0 videoIngest.linux.cfg > cfg.tmp
```

Then copy the contents of `cgf.tmp` into Postman as the value of the `config` parameter.

---

Return to top-level [README](../README.md).
