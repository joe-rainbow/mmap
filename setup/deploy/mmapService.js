/*
 * dependencies
 */
const opts = require('./config.js'),
  fs = require('fs'),
  jp = require('jsonpath'),
  path = require('path'),
  request = require('request'),
  qs = require('querystring');

/*  
 * variables
 */ 
let mmapApiBase = `http://${opts.mmap.host}:${opts.mmap.port}/${opts.mmap.api}/`;

/*  
 * internal methods
 */ 
function mmapAction(verb, data, queryString, callback) {
  let opts = {
    method: verb,
    url: mmapApiBase + queryString
  };

  if(data) { opts.json = data; }

  console.log(opts.method, opts.url);

  request(opts, (error, response, body) => {
    if (error) {
      console.log(error.code);
    } else {
      callback(`${response.statusCode}: ${response.statusMessage}`);
    }
  });
};

function loadConfig(channel) { 
  let configList = [];

  for (let i = 0; i < channel.cfg.length; i++) {
    const cfgText = fs.readFileSync(
      path.join(opts.dirs.configBase, `${channel.cfg[i].template}.json`),
      { encoding: 'utf8' }
    );
    
    const cfgJson = JSON.parse(cfgText); 

    if (channel.cfg[i].hasOwnProperty('override')) {
      for (let j = 0; j < channel.cfg[i].override.length; j++) {
        const param = Object.keys(channel.cfg[i].override[j])[0];
        const value = channel.cfg[i].override[j][param];
        jp.apply(cfgJson, `$..${param}`, currentValue => value);
      }
    }

    configList = configList.concat(cfgJson);
  }

  return configList;
}

function recorderName(channel) {
  return `${qs.escape(channel.name)} Recorder`;
}

/*
 * public methods
 */
function setupChannel(channel) {
  
  let cfg = loadConfig(channel),
    inputData = {
      entityType: channel.type,
      properties: {
        logoSrc: channel.logoSrc
      }
    };
    
  if (!channel.persist) { // file source
    const now = new Date().getTime();

    cfg.push({
      "Ingest": {
        "IngestRate": 0
      },
      "libAVIngest": {
        "IngestDateTime": now
      }
    });

    inputData.properties.defaultTime = now;
  }

  mmapAction('PUT', inputData, `${channel.type.toLowerCase()}s/${qs.escape(channel.name)}`, response => {
    console.log(`Set ${channel.type.toLowerCase()}: ${response}`);

    let recorderData = {
      entityType: 'Video Server Recorder',
      host: opts.recorder.host,
      port: opts.recorder.port,
      rollingBufferBaseUri: `file:///${opts.recorder.baseDir}/encoding/rollingBuffer`,
      eventImagesBaseUri: `file:///${opts.recorder.baseDir}/events`,
      keyframeBaseUri: `file:///${opts.recorder.baseDir}/keyframes`,
      clipBaseUri: `file:///${opts.recorder.baseDir}/clips`
    };

    mmapAction('PUT', recorderData, `recorders/${recorderName(channel)}`, response => {
      console.log(`Set recorder: ${response}`);

      let sourceData = {
        source: channel.name,
        actions: {
          addstream: {
            maxFiles: channel.maxFiles,
            maxFileSizeMB: opts.recorder.maxFileSizeMB
          },
          process: {
            source: channel.src,
            config: cfg,
            persist: channel.persist
          }
        }
      };

      mmapAction('PUT', sourceData, `recorders/${recorderName(channel)}/source`, function (response) {
        console.log(`Assign source: ${response}`);
      });
    });
  });
};

function clearChannel(channel) {
  mmapAction('POST', null, `${channel.type.toLowerCase()}s/${qs.escape(channel.name)}/recorder/stop`, res => {
    console.log(`Stop recorder: ${res}`);

    mmapAction('DELETE', null, `recorders/${recorderName(channel)}/source`, res => {
      console.log(`Unassign source: ${res}`);

      mmapAction('DELETE', null, `recorders/${recorderName(channel)}`, res => {
        console.log(`Delete recorder: ${res}`);

        mmapAction('DELETE', null, `${channel.type.toLowerCase()}s/${qs.escape(channel.name)}`, res => {
          console.log(`Delete ${channel.type.toLowerCase()}: ${res}`);
        });
      });
    });
  });
};

function startRecording(channel) {
  mmapAction('POST', null, `${channel.type.toLowerCase()}s/${qs.escape(channel.name)}/recorder/start`, res => {
    console.log(`Start recorder: ${res}`);
  });
}

function stopRecording(channel) {
  mmapAction('POST', null, `${channel.type.toLowerCase()}s/${qs.escape(channel.name)}/recorder/stop`, res => {
    console.log(`Stop recorder: ${res}`);
  });
}

module.exports = {
  clearChannel: clearChannel,
  setupChannel: setupChannel,
  startRecording: startRecording,
  stopRecording: stopRecording
};
