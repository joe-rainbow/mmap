/*
 * dependencies
 */
let config = require('./config.js'),
  inquirer = require('inquirer'),
  mmapService = require('./mmapService.js');

/*
 * interaction
 */
let questions = [
  {
    type: 'checkbox',
    message: 'On inputs:',
    name: 'inputs',
    choices: config.channels.map(c => c.name)
  },
  {
    type: 'list',
    message: 'Available actions:',
    name: 'actions',
    choices: [
      { name: 'Setup' },
      { name: 'Start' },
      { name: 'Stop' },
      { name: 'Clear' }
    ]
  }
];

inquirer.prompt(questions).then(answers => {
  console.log('Working on it...');
  console.log('\n======================================================================');
  console.log('Check setup here:');
  console.log('http://' + config.mmap.host + ':' + config.mmap.port + '/' + config.mmap.api + '/items');
  console.log('======================================================================\n');

  let selectedChannels = config.channels
    .filter(channel => -1 < answers.inputs.indexOf(channel.name));

  for (let i = 0; i < selectedChannels.length; i++) {
    if (-1 < answers.actions.indexOf('Setup')) { mmapService.setupChannel(selectedChannels[i]); }
    if (-1 < answers.actions.indexOf('Start')) { mmapService.startRecording(selectedChannels[i]); }
    if (-1 < answers.actions.indexOf('Stop')) { mmapService.stopRecording(selectedChannels[i]); }
    if (-1 < answers.actions.indexOf('Clear')) { mmapService.clearChannel(selectedChannels[i]); }
  }
  
});
